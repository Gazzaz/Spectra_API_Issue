
Date of Reporting :27th May 2020

***Vendor, Product***: Sectona Spectra

***Affected Product***: Spectra Version 3.2.0

## Description

Sectona Spectra version 3.2.0 has a vulnerable SOAP API endpoint which leaks sensitive information without proper authentication. This could be used by the unauthorized person to get login credentials to the assets.

## Prove of Concept

The Vulnerable SOAP POST request is displayed in the following screenshot, and the ***pAccountID*** value represents the asset ID, so by just changing it from 1 to 2, or any other number, the login password for that asset will be retrieved.

![sectona](/uploads/d4180082d94bfdc1e3d9b5eebb8b9958/sectona.png)

## Mitigation

Upgrade Sectona Spectra to Version 3.4.0